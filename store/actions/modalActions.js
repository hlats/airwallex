import { MODAL } from '../constants'

const hideModal = (payload) => ({
    type: MODAL.HIDE_MODAL,
    payload
})

const showModal = (payload) => ({
    type: MODAL.SHOW_MODAL,
    payload
})

export {
    hideModal,
    showModal
}