import { REQUEST_INVITATION } from '../constants'

const sendInvitationRequest = (payload) => ({
    type: REQUEST_INVITATION.SEND_REQUEST,
    payload
})

const successfulInvitationRequest = (payload) => ({
    type: REQUEST_INVITATION.SUCCESSFUL_REQUEST,
    payload
})

const failedInvitationRequest = (payload) => ({
    type: REQUEST_INVITATION.FAILED_REQUEST,
    payload
})

const resetInvitationRequest = () => ({
    type: REQUEST_INVITATION.RESET_FORM
})

export {
    sendInvitationRequest,
    successfulInvitationRequest,
    failedInvitationRequest,
    resetInvitationRequest
}