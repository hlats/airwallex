import { MODAL } from '../constants'

//initial state
const initialState = {
    isModalOpen: false,
    modalName: ''
}

//Reducer
const modalReducer = (state = initialState, action = {payload}) => {

    switch(action.type) {
        case MODAL.SHOW_MODAL:
            return {...state, isModalOpen:true, modalName: action.payload }

        case MODAL.HIDE_MODAL:
            return {...state, isModalOpen:false, modalName: action.payload}
    }

    return state
}

export default modalReducer
