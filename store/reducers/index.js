import { combineReducers } from 'redux'
import modalReducer from './modalReducer'
import invitationRequestReducer from './invitationRequestReducer'

const rootReducer = combineReducers({
    modalReducer,
    invitationRequestReducer
})

export default rootReducer
