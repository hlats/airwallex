import { REQUEST_INVITATION } from '../constants'

let formData = {}
let serverResponse = {}

//initial state
const initialState = {
    isLoading: false,
    formData,
    serverResponse
}

//Reducer
const invitationRequestReducer = (state = initialState, action = {}) => {

    switch(action.type) {
        case REQUEST_INVITATION.SEND_REQUEST:
            return {...state, isLoading:true, formData: action.payload}

        case REQUEST_INVITATION.SUCCESSFUL_REQUEST:
            return {...state, isLoading:false, serverResponse: action.payload}

        case REQUEST_INVITATION.FAILED_REQUEST:
            return {...state, isLoading:false, serverResponse: action.payload}

        case REQUEST_INVITATION.RESET_FORM:
            return {...state, isLoading:false, formData: {}, serverResponse: {}}
    }

    return state
}

export default invitationRequestReducer
