import { all } from 'redux-saga/effects'
import watchRequestInvitation from './requestInvitationSaga'

//3.Root saga
//This is the single entry to all sagas => Combines all "watchers"
export default function* rootSaga() {
    yield all([
        watchRequestInvitation()
    ])
}