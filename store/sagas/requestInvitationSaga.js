import { call, put, all, takeEvery } from 'redux-saga/effects'
import { REQUEST_INVITATION } from '../constants'
import { successfulInvitationRequest, failedInvitationRequest } from '../actions'

const api = (url, data) => fetch(url, {
    method: 'post',
    body: JSON.stringify(data)
}).then(res => res.json())

//1.Worker saga
export function* requestInvitationAsync (action) {
    const invitationUrl = 'https://l94wc2001h.execute-api.ap-southeast-2.amazonaws.com/prod/fake-auth'
    
    try {
        const response = yield call(api, invitationUrl, action.payload)
        console.log(response)
        yield put(successfulInvitationRequest(response))
    }
    catch(err) {
        console.log(`WORKER SAGA Error posting invitation request: ${err}`)
        yield put(failedInvitationRequest(err))
    }
}

//2.Watch saga
export default function* watchRequestInvitation() {
    yield takeEvery(REQUEST_INVITATION.SEND_REQUEST, requestInvitationAsync)
}