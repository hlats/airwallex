import React from 'react'
import { render } from "@testing-library/react";
import { Provider } from 'react-redux'
import { createStore as reduxCreateStore, applyMiddleware, compose } from 'redux'
import rootReducer from './store/reducers'
//Sagas setup
import createSagaMiddleware from 'redux-saga'
import rootSaga from './store/sagas/sagas'
const sagaMiddleware = createSagaMiddleware()
const middleWare = applyMiddleware(sagaMiddleware)
const composeEnhancers = ((typeof window !== "undefined") && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose
const createStore = reduxCreateStore(rootReducer, composeEnhancers(middleWare))
sagaMiddleware.run(rootSaga)

// Add in any providers here if necessary:
// (ReduxProvider, ThemeProvider, etc)
const Providers = ({ children }) => {
    return <Provider store={createStore}>{children}</Provider>;
};

const customRender = (ui, options = {}) =>
    render(ui, { wrapper: Providers, ...options });

// re-export everything
export * from "@testing-library/react";

// override render method
export { customRender as render };