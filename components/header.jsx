import React from 'react'
import { iconBroccoli } from '../icons'

const Header = () => {
    return (
        <header id='main-header' data-testid='main-header'>
            <div className='content-wrapper'>
                <h1 className='site-name'><span className='icon'>{iconBroccoli}</span> Broccoli &amp; Co.</h1>
            </div>
        </header>
    )
}

export default Header
