import React from 'react'
import { iconHeart } from '../icons'

const Footer = () => {
    const year = new Date().getFullYear()
    
    return (
        <footer id='main-footer' data-testid='main-footer'>
            <div className='content-wrapper disclaimer'>
                <p>Made with <span className='icon'>{iconHeart}</span> in Melbourne</p>
                <p className='copyright'>&copy; {year} Broccoli &amp; Co. All rights reserved.</p>
            </div>
        </footer>
    )
}

export default Footer