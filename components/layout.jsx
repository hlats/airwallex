import React from 'react'
import { useSelector } from "react-redux"
import Header from './header'
import Footer from './footer'
import RequestDialog from './request-dialog'

const Layout = ({children}) => {
    const store = useSelector(store => store)
    const isModalOpen = store.modalReducer.isModalOpen
    const modalName = store.modalReducer.modalName
    
    return (
        <div id='site-wrapper'>
            <Header />
            {children}
            <Footer />
            {isModalOpen && (modalName === 'request-invitation') && <RequestDialog modal_name='request-invitation' />}
        </div>
    )
}

export default Layout
