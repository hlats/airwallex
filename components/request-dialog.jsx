import React from 'react'
import { useState } from 'react'
import { useSelector, useDispatch } from "react-redux"
import { useForm } from 'react-hook-form'
import Dialog from './shared/dialog'
import { sendInvitationRequest, hideModal, resetInvitationRequest } from '../store/actions'

const RequestDialog = (props) => {
    const {modal_name} = props

    const store = useSelector(store => store)
    const isLoading = store.invitationRequestReducer.isLoading
    const serverErrorResponse = store.invitationRequestReducer.serverResponse.errorMessage
    const registrationSuccessful = store.invitationRequestReducer.serverResponse === 'Registered'
    
    const dispatch = useDispatch()

    const [formName, setFormName] = useState('')
    const [formEmail, setFormEmail] = useState('')
    const [formConfirmEmail, setFormConfirmEmail] = useState('')

    const { register, handleSubmit, errors } = useForm()

    const onSubmit = (data) => {
        dispatch(sendInvitationRequest(data))
    }

    const closeDialog = () => {
        dispatch(hideModal(modal_name))
        dispatch(resetInvitationRequest())
    }

    return (
        <Dialog title={registrationSuccessful ? 'All done' : 'Request an invite'} hasHeader={true} hasFooter={false} modal_name={modal_name}>
            <div id='request-dialog' data-testid='request-dialog'>
                {!registrationSuccessful && (
                    <form data-testid='request-form' onSubmit={handleSubmit(onSubmit)}>
                        <input type='text' name='name' placeholder='Full name' aria-label='Full name' className={errors.name && 'has-error'} value={formName} onChange={(e) => { setFormName(e.target.value) }} ref={register({ required: true, minLength: 3 })} />
                        {errors.name && errors.name.type === 'required' && <span className='error-msg'><i className='fa fa-exclamation-triangle' aria-hidden='true'></i>Full name is required</span>}
                        {errors.name && errors.name.type === 'minLength' && <span className='error-msg'><i className='fa fa-exclamation-triangle' aria-hidden='true'></i>Minimun length of 3 letters</span>}

                        <input type='text' name='email' placeholder='Email' aria-label='Email' className={errors.email && 'has-error'} value={formEmail} onChange={(e) => { setFormEmail(e.target.value) }} ref={register({ required: true, pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ })} />
                        {errors.email && errors.email.type === 'required' && <span className='error-msg'><i className='fa fa-exclamation-triangle' aria-hidden='true'></i>Email is required</span>}
                        {errors.email && errors.email.type === 'pattern' && <span className='error-msg'><i className='fa fa-exclamation-triangle' aria-hidden='true'></i>Please enter a valid email address</span>}

                        <input type='text' name='confirmemail' placeholder='Confirm Email' aria-label='Confirm Email' className={errors.confirmemail && 'has-error'} value={formConfirmEmail} onChange={(e) => { setFormConfirmEmail(e.target.value) }} ref={register({ required: true, pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ })} />
                        {errors.confirmemail && errors.confirmemail.type === 'required' && <span className='error-msg'><i className='fa fa-exclamation-triangle' aria-hidden='true'></i>Email confirmation is required</span>}
                        {errors.confirmemail && errors.confirmemail.type === 'pattern' && <span className='error-msg'><i className='fa fa-exclamation-triangle' aria-hidden='true'></i>Please enter a valid email address</span>}
                        {errors.confirmemail && (formEmail !== formConfirmEmail) && <span className='error-msg'><i className='fa fa-exclamation-triangle' aria-hidden='true'></i>Email do not match</span>}

                        <button className={`btn ${isLoading ? 'btn-standard disabled' : 'btn-primary'}`} type='submit'>
                            {isLoading ? 'Sending, please wait...' : 'Send'}
                        </button>

                        {serverErrorResponse && <p className='server-response error-msg'>{serverErrorResponse}</p>}
                    </form>
                )}

                {registrationSuccessful && (
                    <div className='success-confirmation'>
                        <p>
                            You will be one of the first to experience Broccoli &amp; Co. when we launch.
                        </p>

                        <button className='btn btn-standard' onClick={() => {closeDialog()}}>OK</button>
                    </div>
                )}
            </div>
        </Dialog>
    )
}

export default RequestDialog
