import React from 'react'
import { useDispatch } from "react-redux"
import { motion } from 'framer-motion'
import { hideModal, resetInvitationRequest } from '../../store/actions'
import { fadeInDown } from '../../animations'
import { iconCross } from '../../icons'

const Dialog = (props) => {
    const { children, title, hasHeader, hasFooter, modal_name } = props
    const dispatch = useDispatch()

    const closeDialog = () => {
        dispatch(hideModal(modal_name))
        dispatch(resetInvitationRequest())
    }

    return (
        <motion.div data-testid='dialog' exit={{ opacity: 0.5 }} initial={{ opacity: 0.5 }} animate={{ opacity: 1 }}>
            <motion.div className='dialog-wrapper' initial='initial' animate='animate'>
                <div className='backdrop' onClick={() => { closeDialog() }}></div>
                <motion.div className='modal' variants={fadeInDown}>
                    {hasHeader && (
                        <div className='modal-header'>
                            <h3>{title}</h3>
                            <button className='close-modal' onClick={() => { closeDialog() }}><span className='icon'></span>{iconCross}</button>
                            <hr />
                        </div>
                    )}

                    <div className='modal-body'>
                        {children}
                    </div>

                    {hasFooter && (
                        <div className='modal-footer'>
                            <button className='btn btn-standard' onClick={() => { closeDialog() }}>Close</button>
                        </div>
                    )}
                </motion.div>
            </motion.div>
        </motion.div>
    )
}

export default Dialog
