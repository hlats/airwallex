import React from 'react'
import '../styles/globals.scss'

//Animation
import { AnimatePresence } from 'framer-motion'

//Redux setup
import { Provider } from 'react-redux'
import { createStore as reduxCreateStore, applyMiddleware, compose } from 'redux'
import rootReducer from '../store/reducers'

//Sagas setup
import createSagaMiddleware from 'redux-saga'
import rootSaga from '../store/sagas/sagas'
const sagaMiddleware = createSagaMiddleware()
const middleWare = applyMiddleware(sagaMiddleware)

const composeEnhancers = ((typeof window !== "undefined") && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose
const createStore = reduxCreateStore(rootReducer, composeEnhancers(middleWare))


sagaMiddleware.run(rootSaga)

function MyApp({ Component, pageProps }) {
	return (
		<Provider store={createStore}>
			<AnimatePresence exitBeforeEnter>
				<Component {...pageProps} />
			</AnimatePresence>
		</Provider>
	)
}

export default MyApp
