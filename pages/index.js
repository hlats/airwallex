import React from 'react'
import Head from 'next/head'
import { useDispatch } from "react-redux"
import { motion } from 'framer-motion'
import Layout from '../components/layout'
import { showModal } from '../store/actions/'
import { fadeInDown } from '../animations'

const HomePage = () => {
	const dispatch = useDispatch()

	const openModal = () => {
		dispatch(showModal('request-invitation'))
	}

	return (
		<Layout>
			<Head>
				<title>Broccoli &amp; Co.</title>
			</Head>
			<motion.main id='home-page' data-testid='home-page' exit={{ opacity: 0.5 }} initial={{ opacity: 0.5 }} animate={{ opacity: 1 }}>
				<motion.div className='content-wrapper' initial='initial' animate='animate'>
					<motion.div variants={fadeInDown}>
						<h1>A better way<br />to enjoy every day.</h1>

						<p>Be the first to know when we launch.</p>

						<button data-testid='request-invite-btn' className='btn btn-primary' onClick={() => { openModal() }}>Request an invite</button>
					</motion.div>
				</motion.div>
			</motion.main>
		</Layout>
	)
}

export default HomePage