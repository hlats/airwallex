const easing = [0.6, -0.05, 0.01, 0.99]

const fadeInDown = {
	initial: {
		y: -30,
		opacity: 0
	},
	animate: {
		y: 0,
		opacity: 1,
		transition: {
			duration: 0.6,
			ease: easing
		}
	}
}

const stagger = {
	animate: {
		transition: {
			staggerChildren: 0.1
		}
	}
}

export {
    easing,
    fadeInDown,
    stagger
}