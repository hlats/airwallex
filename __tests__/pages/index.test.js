import React from 'react'
import { fireEvent, cleanup } from '@testing-library/react'
import '@testing-library/jest-dom'
import { render } from '../../test-utils'
import HomePage from '../../pages/index'

describe('Home Page', () => {
    afterEach(cleanup)

    it('should render without crashing', () => {
        const { getByTestId } = render(<HomePage />)
        expect(getByTestId('home-page')).toBeInTheDocument()
    })

    it('should render a request invitation button', () => {
        const { getByTestId } = render(<HomePage />)
        expect(getByTestId('home-page')).toContainElement(getByTestId('request-invite-btn'))
    })
})