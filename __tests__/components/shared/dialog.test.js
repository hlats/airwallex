import React from 'react'
import { fireEvent, cleanup } from '@testing-library/react'
import '@testing-library/jest-dom'
import { render } from '../../../test-utils'
import Dialog from '../../../components/shared/dialog'

describe('Request invitation dialog', () => {
    afterEach(cleanup)

    it('should render without crashing', () => {
        const { getByTestId } = render(<Dialog />)
        expect(getByTestId('dialog')).toBeInTheDocument()
    })
})