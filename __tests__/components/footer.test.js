import React from 'react'
import '@testing-library/jest-dom'
import { render } from '../../test-utils'
import Footer from '../../components/footer'

describe('Footer', () => {
    it('should have rendered', () => {
        const { getByTestId } = render(<Footer />)
        expect(getByTestId('main-footer')).toBeInTheDocument
    })
})