import React from 'react'
import { fireEvent, cleanup } from '@testing-library/react'
import '@testing-library/jest-dom'
import { render } from '../../test-utils'
import RequestDialog from '../../components/request-dialog'

describe('Request invitation dialog', () => {
    afterEach(cleanup)

    it('should render without crashing', () => {
        const { getByTestId } = render(<RequestDialog />)
        expect(getByTestId('request-dialog')).toBeInTheDocument()
    })

    it('should contain a request invitation form', () => {
        const { getByTestId } = render(<RequestDialog />)
        expect(getByTestId('request-dialog')).toContainElement(getByTestId('request-form'))
    })
})