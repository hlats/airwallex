import React from 'react'
import '@testing-library/jest-dom'
import { render } from '../../test-utils'
import Header from '../../components/header'

describe('Footer', () => {
    it('should have rendered', () => {
        const { getByTestId } = render(<Header />)
        expect(getByTestId('main-header')).toBeInTheDocument
    })
})